title: Spring Boot 实验场 
date: 2019-05-25 20:34:07
tags: Spring Boot
---
在 _Spring Boot_ 之上，全面使用 _Java_ 注解代替 _XML_ 配置。用最便捷的方法，实现一些基础框架功能。
<!--more-->

## 1. Mybatis

   使用 _mybatis-spring-boot-starter_ ， 它为 _Spring Boot_ 实现了autoconfig和基于注解的mapper配置方式。

   ### 基本用法：（数据源和链接池配置省略）
   
   首先，引入 _mybatis-spring-boot-starter_ ，不用额外的配置；
   ``` gradle gradle.xml
   compile 'org.mybatis.spring.boot:mybatis-spring-boot-starter:1.2.0'
   ```
   之后，根据表结构编写 _bean_ ；

   最后是编写 _mapper_ ，因为 _XML_ 被 _Java_ 注释(@Select,@Insert,@Update,@Delete……)代替，所以全部的逻辑回到这里，例：
   ``` java UserMapper.java
    @Mapper
    public interface UserMapper {
        @Select("select * from user where id=#{id}")
        User selectByPrimaryKey(@Param("id") int id);

        @Select("select id,name from user")
        List<User> allUsers();

        @Insert("insert into user(name,password) value(#{name},#{password})")
        int insertUser(User user);

        @SelectProvider(type = UserSqlBuilder.class, method = "buildGetUserByIds")
        List<User> selectByIds(@Param("idList") List<Integer> idList);
    }
   ```
   至于动态 _sql_ ，使用@SelectProvider、@InsertProvider、@UpdateProvider、@DeleteProvider可以实现，下面是例子（从上面的例子可以看出它是怎么使用的）：
   ``` java UserSqlBuilder.java
    public class UserSqlBuilder {
        public String buildGetUserByIds(@Param("idList") final List<Integer> idList){
            return new SQL(){{
                SELECT("id","name");
                FROM("user");
                if (idList!=null){
                    String ids = idList.stream()
                    .map(Object::toString)
                    .collect(Collectors.joining(","));
                    WHERE("id in ("+ids+")");
                }
            }}.toString();
        }
    }
   ```
   
   ### 进阶-拦截器
   #### 分表
   使用 _Mybatis_ 拦截器在org.apache.ibatis.executor.statement.StatementHandler#prepare拦截应该是最简单的，可以方便的拿到数据库类型、操作SQL……，实现org.apache.ibatis.plugin.Interceptor，详见[TableRouteInterceptor](https://gitlab.com/thelostsoul/SpringBootFirst/blob/master/server/src/main/java/xyz/thelostsoul/base/TableRouteInterceptor.java)。这里使用了 _Druid_ 的SQL Parser，分析表名并替换。

## 4. 多数据源
首先是 _Spring_ 提供了接口AbstractRoutingDataSource，可以方便的实现多数据源，如下：
``` java
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

public class MultipleDataSource extends AbstractRoutingDataSource {
    @Override
    protected Object determineCurrentLookupKey() {
        Database database = DatabaseContextHolder.getDatabase();
        if (database == null) {
            database = Database.primary;
        }
        return database;
    }
}
```

该数据源的使用配置如下：
``` java
@Bean(name = "multiDataSource")
public DataSource multiDataSource(@Qualifie("primaryDataSource") DataSource primaryDataSource,
                                  @Qualifier("secondDataSource") DataSource secondDataSource) {
    Map targetDataSources = new HashMap();
    targetDataSources.put(Database.primary, primaryDataSource);
    targetDataSources.put(Database.second, secondDataSource);
    MultipleDataSource multipleDataSource = new MultipleDataSource();
    multipleDataSource.setTargetDataSources(targetDataSources);
    multipleDataSource.setDefaultTargetDataSource(primaryDataSource);
    return multipleDataSource;
}
```

其中xyz.thelostsoul.base.MultipleDataSource#determineCurrentLookupKey()的返回确定了targetDataSources的key，最后会映射出数据源。但是此时multiDataSource，因为mybatis-spring-boot-starter的AutoConfig不会处理配置中有多个数据源的情况，需要自行把multiDataSource注入到SqlSessionFactory中去。
至于数据源的切换，看各自的实现方式，我的实现是：Mapper上注解，切面拦截注解配置保存到ThreadLocal，如下：
``` java
@Aspect
@Configuration
public class DataSourceSetAspect {

    @Pointcut("execution(* xyz.thelostsoul..dao.*Mapper.*(..))||execution(* xyz.thelostsoul..dao.*DAO.*(..))")
    public void setPoint() {}

    @Around("setPoint()")
    public Object handler(ProceedingJoinPoint joinPoint) throws Throwable {
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();

        DataSourceSetter sourceSetter = method.getAnnotation(DataSourceSetter.class);
        if (sourceSetter == null) {
            Class<?> clazz = method.getDeclaringClass();
            sourceSetter = clazz.getAnnotation(DataSourceSetter.class);
        }

        try {
            if (sourceSetter != null) {
                //保存到ThreadLocal
                DatabaseContextHolder.setDatabase(sourceSetter.value());
            }
            return joinPoint.proceed();
        } finally {
            DatabaseContextHolder.clearDatabase();
        }
    }
}
```

数据源有多个的情况下，事务控制也应该覆盖多个数据源，需要重新实现org.apache.ibatis.transaction.Transaction，详见
[MultipleDataSourceTransaction](https://gitlab.com/thelostsoul/SpringBootFirst/blob/master/server/src/main/java/xyz/thelostsoul/base/MultipleDataSourceTransaction.java)

## 3. profile
使用 _Spring_ 的这个特性，可以管理、切换多个环境的配置;结合@Profile的使用更可以定制适用多个环境的代码。

## 4. 单元测试
个人觉得单元测试也应该分层，待续……

   ### 使用h2做DAO层测试

   ### 使用mockito做SV层测试